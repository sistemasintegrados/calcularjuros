﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using TaxaJurosDAL.ApiRest;
using TaxaJurosDAL.Models;

namespace TaxaJurosDAL.DAO
{
    public static class CalculaJurosDAO
    {
        public static double Resultado(CalculaJuros calculaJuros)
        {
            calculaJuros.Juros = RetornaTaxaJuros();
            return Math.Round((Convert.ToDouble(calculaJuros.ValorInicial) * Math.Pow(1 + calculaJuros.Juros, Convert.ToDouble(calculaJuros.Meses))), 2);
        }

       
        private static Double RetornaTaxaJuros()
        {       
            HttpClient clientTaxaJuros = ConfiguraApiRest.Configura("https://localhost:44324/");
            HttpResponseMessage response = clientTaxaJuros.GetAsync("TaxaJuros").Result;
            var dadosResult = response.Content.ReadAsStringAsync();
            //var resposta = JsonConvert.DeserializeObject<MensagemValidacaoResponse>(dadosResult.Result);

            return Convert.ToDouble(dadosResult.Result);
        }

    }
}
