﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxaJurosDAL.Models
{
    public class CalculaJuros
    {
        public decimal ValorInicial { get; set; }
        public double Juros { get; set; }
        public int Meses { get; set; }
    }
}
