﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaxaJurosDAL.DAO;
using TaxaJurosDAL.Models;

namespace CalcularJuros.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CalculaJurosController : ControllerBase
    {
        // POST 
        [HttpPost]
        public ActionResult Post([FromQuery] CalculaJuros calculaJuros)
        { 
            var result = CalculaJurosDAO.Resultado(calculaJuros);
            return Ok(result);
        }
    }
}