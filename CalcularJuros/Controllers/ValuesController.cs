﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CalcularJuros.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET /values
        [HttpGet()]
        public ActionResult<string> Get()
        {
            return "Api para calculo de juros";
        }
    }
}
