﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalcularJuros.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ShowMeTheCodeController : ControllerBase
    {
        // POST 
        [HttpGet]
        public ActionResult Get()
        {
            var result = "https://gitlab.com/sistemasintegrados/calcularjuros.git";
            return Ok(result);
        }
    }
}