﻿using Microsoft.AspNetCore.Mvc;
using TaxaJurosDAL.DAO;

namespace CalcularJuros.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TaxaJurosController : ControllerBase
    {
        // POST 
        [HttpGet]
        public ActionResult Get()
        {
            var result = TaxaJurosDAO.ValorTaxaJurosPadrao();
            return Ok(result);
        }

   
    }
}